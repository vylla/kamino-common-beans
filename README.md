# Common bean configuration #

This is the common beans library, it provides common utilities to be used by all microservices.

All spring application basic configuration should be placed in this library to reduce code duplication between microservices.


package com.mahisoft.kamino.commonbeans.logging.formatter;

import ch.qos.logback.contrib.json.JsonFormatter;
import com.google.gson.Gson;

import java.util.Map;

public class GSONFormatter implements JsonFormatter {
    private Gson gson;

    public GSONFormatter() {
        this.gson = new Gson();
    }

    @Override
    public String toJsonString(Map m) {
        return gson.toJson(m);
    }
}

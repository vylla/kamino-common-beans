package com.mahisoft.kamino.commonbeans.logging.layout;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.pattern.ThrowableProxyConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.JsonLayoutBase;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import com.mahisoft.kamino.commonbeans.logging.aspect.LoggerAspect;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StackdriverLayout extends JsonLayoutBase<ILoggingEvent> {

    private final ThrowableProxyConverter tpc;

    private static final Map<String, String> LEVEL_MAPPING = ImmutableMap
            .<String, String>builder()
            .put(Level.WARN.levelStr, "WARNING")
            .put(Level.ERROR.levelStr, "ERROR")
            .put(Level.INFO.levelStr, "INFO")
            .build();

    public StackdriverLayout() {
        this.tpc = new ThrowableProxyConverter();
    }

    @Override
    public void start() {
        tpc.start();
        super.start();
    }

    @Override
    public void stop() {
        tpc.stop();
        super.stop();
    }

    @Override
    protected Map toJsonMap(ILoggingEvent event) {
        Map<Object, Object> entry = new HashMap<>();

        entry.put("severity", levelToSeverity(event.getLevel()));
        entry.put("timestamp", event.getTimeStamp());
        entry.put("message", getMessage(event));

        if (!CollectionUtils.isEmpty(event.getMDCPropertyMap())) {
            entry.put("details", event.getMDCPropertyMap()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey,
                            x -> LoggerAspect.MDC_EXECUTION_TIME.equals(x.getKey()) ?
                                    Long.valueOf(x.getValue()) : x.getValue())));
        }
        setRequestIdentifier(entry);
        setSourceLocation(entry, event);
        return entry;
    }

    private String getMessage(ILoggingEvent event) {
        String message = event.getFormattedMessage();
        String stackTrace = tpc.convert(event);
        if (!Strings.isNullOrEmpty(stackTrace)) {
            return message + "\n" + stackTrace;
        }
        return message;
    }

    private void setRequestIdentifier(Map<Object, Object> entry) {
        if (!Strings.isNullOrEmpty(RequestIdentifier.getIdentifier())) {
            entry.put("logging.googleapis.com/trace", RequestIdentifier.getIdentifier());
        }
    }

    private static void setSourceLocation(Map<Object, Object> entry, ILoggingEvent event) {
        StackTraceElement[] cda = event.getCallerData();
        if (cda != null && cda.length > 0) {
            Map<String, Object> sourceLocation = new HashMap<>();
            StackTraceElement ste = cda[0];
            sourceLocation.put("function", ste.getClassName() + "." + ste.getMethodName() + (ste.isNativeMethod() ? "(Native Method)" : ""));
            if (ste.getFileName() != null) {
                String pkg = ste.getClassName().replaceAll("\\.", "/");
                pkg = pkg.substring(0, pkg.lastIndexOf('/') + 1);
                sourceLocation.put("file", pkg + ste.getFileName());
            }
            sourceLocation.put("line", ste.getLineNumber());
            entry.put("logging.googleapis.com/sourceLocation", sourceLocation);
        }
    }

    private String levelToSeverity(Level level) {
        return LEVEL_MAPPING.getOrDefault(level.levelStr,
                "DEBUG");
    }
}

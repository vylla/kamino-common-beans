package com.mahisoft.kamino.commonbeans.logging.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Loggable {

    /**
     * Sets the category or classification of the annotated type. Ex: Controller, Service, Repository
     *
     * @return Category name
     */
    String category() default LoggableCategories.UNKNOWN;
}
package com.mahisoft.kamino.commonbeans.logging.aspect;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "kamino.commons.logging")
@Data
public class LoggerConfiguration {

    @Data
    public class ExecutionTimeWarning {

        private Map<String, Integer> toleranceByCategory = new HashMap<>();

        private Map<String, Integer> toleranceByClass = new HashMap<>();

        private Map<String, Integer> toleranceByMethod = new HashMap<>();

        private Integer defaultTolerance = 1000;

        public Integer getTolerance(String category, String className, String methodName) {

            return toleranceByMethod.getOrDefault(
                    StringUtils.join(className, ".", methodName),
                    toleranceByClass.getOrDefault(
                            className, toleranceByCategory.getOrDefault(
                                    category, defaultTolerance)));
        }

    }

    private ExecutionTimeWarning executionTimeWarning = new ExecutionTimeWarning();

}

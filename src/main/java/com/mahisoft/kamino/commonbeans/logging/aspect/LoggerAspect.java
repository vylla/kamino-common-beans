package com.mahisoft.kamino.commonbeans.logging.aspect;


import com.mahisoft.kamino.commonbeans.exception.ApiHttpClientErrorException;
import com.mahisoft.kamino.commonbeans.exception.ApiHttpServerErrorException;
import com.mahisoft.kamino.commonbeans.exception.CustomValidationException;
import com.mahisoft.kamino.commonbeans.exception.ResourceConflictException;
import com.mahisoft.kamino.commonbeans.exception.ResourceForbiddenException;
import com.mahisoft.kamino.commonbeans.exception.ServiceTempUnavailableException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Aspect
@Component
@ConditionalOnProperty("kamino.commons.logging.enabled")
@RequiredArgsConstructor
public class LoggerAspect {


    public static final String MDC_EVENT = "event";
    public static final String MDC_CATEGORY = "category";
    public static final String MDC_EXECUTION_TIME = "executionTime";

    private enum InternalLevel {
        NORMAL(0),
        WARN(1),
        ERROR(2);

        private int value;

        InternalLevel(int value) {

            this.value = value;
        }

        public int value() {
            return this.value;
        }
    }

    private final LoggerConfiguration configuration;

    @Pointcut(value = "@within(loggable)")
    public void typeAnnotated(Loggable loggable) {
        // Defining pointcut for classes annotated with Loggable
    }

    @Pointcut("execution(public * *(..))")
    public void publicMethods() {
        // Defining pointcut for public methods
    }

    @Around(value = "execution(public * org.springframework.data.repository.Repository+.*(..))")
    public Object logMethod(final ProceedingJoinPoint joinPoint) throws Throwable {

        Optional<Class<?>> optionalClass = Stream.of(joinPoint.getTarget().getClass().getInterfaces())
                .filter(x -> Objects.nonNull(x.getAnnotation(Loggable.class)))
                .findFirst();

        if (optionalClass.isPresent()) {
            Class<?> aClass = optionalClass.get();
            return log(aClass, joinPoint, aClass.getAnnotation(Loggable.class));
        }
        return joinPoint.proceed();
    }

    @Around(value = "publicMethods() && typeAnnotated(loggable)")
    public Object logAllMethodsOfType(final ProceedingJoinPoint joinPoint, final Loggable loggable) throws Throwable {
        return log(joinPoint.getTarget().getClass(), joinPoint, loggable);
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private Object log(Class<?> targetClass, ProceedingJoinPoint joinPoint, Loggable loggable) throws Throwable {
        Logger logger = LoggerFactory.getLogger(targetClass);

        Marker marker = MarkerFactory.getMarker(loggable.category());

        long start = System.currentTimeMillis();

        long executionTimeTolerance = configuration.getExecutionTimeWarning().getTolerance(
                loggable.category(), targetClass.getName(), joinPoint.getSignature().getName()
        );

        enter(logger, marker, joinPoint, loggable.category());

        Object returnValue = null;
        String errorMessage = null;
        InternalLevel internalLevel = InternalLevel.NORMAL;
        try {
            returnValue = joinPoint.proceed();
        } catch (NoSuchElementException | IllegalArgumentException | IllegalStateException |
                CustomValidationException | ResourceConflictException | ResourceForbiddenException |
                ServiceTempUnavailableException ex) {
            internalLevel = InternalLevel.WARN;
            errorMessage = ex.getMessage();
            throw ex;
        } catch (ApiHttpClientErrorException | ApiHttpServerErrorException ex) {
            internalLevel = InternalLevel.ERROR;
            errorMessage = String.format("HTTP API ERROR %s:\n%s", ex.getMessage(), ex.getResponseBodyAsString());
            throw ex;
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            internalLevel = InternalLevel.ERROR;
            errorMessage = String.format("HTTP ERROR %s:\n%s", ex.getMessage(), ex.getResponseBodyAsString());
            throw ex;
        } catch (Exception ex) {
            internalLevel = InternalLevel.ERROR;
            errorMessage = ex.getMessage();
            throw ex;
        } finally {
            long execution = System.currentTimeMillis() - start;
            internalLevel = executionTimeTolerance > 0 && execution > executionTimeTolerance && internalLevel.value() < InternalLevel.WARN.value() ? InternalLevel.WARN : internalLevel;
            exit(logger, marker, joinPoint, internalLevel, loggable.category(), execution, errorMessage, returnValue);
        }

        return returnValue;
    }

    private static void enter(Logger logger, Marker marker, ProceedingJoinPoint joinPoint, String category) {
        if (!logger.isTraceEnabled() && !logger.isDebugEnabled()) {
            return;
        }

        try {
            MDC.put(MDC_EVENT, "ENTER");
            MDC.put(MDC_CATEGORY, category);
            if (logger.isTraceEnabled()) {
                logger.trace(marker, "{} ENTER {}\n{}", category, joinPoint.getSignature().toShortString(),
                        StringUtils.join(joinPoint.getArgs(), '\n')
                );
            } else {
                logger.debug(marker, "{} ENTER {}", category, joinPoint.getSignature().toShortString());
            }
        } finally {
            MDC.remove(MDC_EVENT);
            MDC.remove(MDC_CATEGORY);
        }
    }

    private static void exit(Logger logger, Marker marker, ProceedingJoinPoint joinPoint, InternalLevel internalLevel, String category,
                             long executionTime, String message, Object returnValue) {

        if (internalLevel.value() == InternalLevel.NORMAL.value() && !logger.isTraceEnabled() && !logger.isDebugEnabled()) {
            return;
        }

        try {
            MDC.put(MDC_EVENT, "EXIT");
            MDC.put(MDC_CATEGORY, category);
            MDC.put(MDC_EXECUTION_TIME, String.valueOf(executionTime));

            switch (internalLevel) {
                case WARN:
                    logger.warn(marker, "{} EXIT {} {}ms {}", category, joinPoint.getSignature().toShortString(), executionTime, message);
                    break;
                case ERROR:
                    logger.error(marker, "{} EXIT {} {}ms {}", category, joinPoint.getSignature().toShortString(), executionTime, message);
                    break;
                default:
                    if (logger.isTraceEnabled()) {
                        logger.trace(marker, "{} EXIT {} {}ms {}\n{}", category, joinPoint.getSignature().toShortString(), executionTime, message, returnValue);
                    } else {
                        logger.debug(marker, "{} EXIT {} {}ms {}", category, joinPoint.getSignature().toShortString(), executionTime, message);
                    }
                    break;
            }
        } finally {
            MDC.remove(MDC_EVENT);
            MDC.remove(MDC_CATEGORY);
            MDC.remove(MDC_EXECUTION_TIME);
        }
    }
}
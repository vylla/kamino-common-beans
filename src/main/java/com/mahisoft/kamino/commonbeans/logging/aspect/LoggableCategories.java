package com.mahisoft.kamino.commonbeans.logging.aspect;

/**
 * Common name of an application layer used as category with Loggable annotation
 * <p>
 * Ex: Loggable(category = LoggableCategories.CONTROLLER)
 */
public final class LoggableCategories {

    private LoggableCategories() {
    }

    public static final String CONTROLLER = "CONTROLLER";
    public static final String SERVICE = "SERVICE";
    public static final String REPOSITORY = "REPOSITORY";
    public static final String CLIENT = "CLIENT";
    public static final String DAEMON = "DAEMON";
    public static final String LISTENER = "LISTENER";
    public static final String UNKNOWN = "UNKNOWN";

}

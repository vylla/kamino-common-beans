package com.mahisoft.kamino.commonbeans.audit;

import com.mahisoft.kamino.commonbeans.auth.AuthenticationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = {"oauth2.auth.jwt.keyValue", "spring.datasource.url"})
@RequiredArgsConstructor
public class Auditor implements AuditorAware<Long> {

    private final AuthenticationHelper authenticationHelper;

    public Long getCurrentAuditor() {
        try {
            return authenticationHelper.getUserId().orElse(0L);
        } catch (InvalidTokenException ex) {
            return 0L;
        }
    }
}
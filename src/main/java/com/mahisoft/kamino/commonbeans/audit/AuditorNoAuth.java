package com.mahisoft.kamino.commonbeans.audit;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Auditor bean that only returns 0l.
 * Meant to be used for services with authEnabled: false
 */
@Component
@ConditionalOnProperty(value = "spring.authentication.authEnabled", havingValue = "false")
public class AuditorNoAuth implements AuditorAware<Long> {

    public Long getCurrentAuditor() {
        return 0L;
    }
}
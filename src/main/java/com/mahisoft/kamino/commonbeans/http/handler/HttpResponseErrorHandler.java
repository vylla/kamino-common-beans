package com.mahisoft.kamino.commonbeans.http.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mahisoft.kamino.commonbeans.exception.ApiHttpClientErrorException;
import com.mahisoft.kamino.commonbeans.exception.ApiHttpServerErrorException;
import com.mahisoft.kamino.commonbeans.http.ApiError;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.io.IOException;
import java.nio.charset.Charset;

@RequiredArgsConstructor
public class HttpResponseErrorHandler extends DefaultResponseErrorHandler {

    private final ObjectMapper objectMapper;

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        try {
            super.handleError(response);
        } catch (HttpClientErrorException e) {
            overrideClientErrorException(e, response);
        } catch (HttpServerErrorException e) {
            overrideServerErrorException(e, response);
        }
    }

    private void overrideClientErrorException(HttpClientErrorException e, ClientHttpResponse response) {
        try {
            ApiError error = objectMapper.readValue(e.getResponseBodyAsString(), ApiError.class);
            throw new ApiHttpClientErrorException(e, error, getCharset(response));
        } catch (IOException ex) {
            throw e;
        }
    }

    private void overrideServerErrorException(HttpServerErrorException e, ClientHttpResponse response) {
        try {
            ApiError error = objectMapper.readValue(e.getResponseBodyAsString(), ApiError.class);
            throw new ApiHttpServerErrorException(e, error, getCharset(response));
        } catch (IOException ex) {
            throw e;
        }
    }

    @Override
    protected Charset getCharset(ClientHttpResponse response) {
        HttpHeaders headers = response.getHeaders();
        MediaType contentType = headers.getContentType();
        return contentType != null ? contentType.getCharset() : null;
    }
}

package com.mahisoft.kamino.commonbeans.http;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;

import java.time.Instant;
import java.util.List;


public class ValidationError extends ApiError {

    private Object requestBody;

    private List<ObjectError> errors;

    public ValidationError(Throwable t, Object requestBody, String code, List<ObjectError> errors) {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(), t.getMessage(), HttpStatus.BAD_REQUEST.value(), Instant.now(), code);
        this.requestBody = requestBody;
        this.errors = errors;
    }

    @JsonGetter
    public Object getRequestBody() {
        return requestBody;
    }

    @JsonGetter
    public List<ObjectError> getErrors() {
        return errors;
    }
}


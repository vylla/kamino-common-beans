package com.mahisoft.kamino.commonbeans.http.interceptor;

import com.google.common.base.Strings;
import com.mahisoft.kamino.commonbeans.http.Headers;
import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class HeaderRequestInterceptor implements ClientHttpRequestInterceptor {

    @Value("${spring.application.name:}")
    private String serviceName;

    @Override
    public ClientHttpResponse intercept(HttpRequest req, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = req.getHeaders();

        if (!headers.containsKey(Headers.REQUEST_IDENTIFIER) && RequestIdentifier.getIdentifier() != null) {
            headers.add(Headers.REQUEST_IDENTIFIER, RequestIdentifier.getIdentifier());
        }

        if (!Strings.isNullOrEmpty(serviceName)) {
            headers.add(Headers.REQUEST_SERVICE_NAME, serviceName);
        }

        return execution.execute(req, body);
    }
}
package com.mahisoft.kamino.commonbeans.http;

public final class Headers {

    private Headers() {

    }

    public final static String REQUEST_IDENTIFIER = "X-Request-ID";
    public final static String REQUEST_SERVICE_NAME = "X-Request-Service-Name";

    public final static String AUTHORIZATION = "Authorization";


}

package com.mahisoft.kamino.commonbeans.http;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FieldError {

    private String fieldName;

    private String description;
}

package com.mahisoft.kamino.commonbeans.http;

public final class RequestIdentifier {

    private RequestIdentifier() {
    }

    private static final ThreadLocal<String> IDENTIFIER = new ThreadLocal<>();

    public static String getIdentifier() {
        return IDENTIFIER.get();
    }

    public static void setIdentifier(String identifier) {
        IDENTIFIER.set(identifier);
    }
}

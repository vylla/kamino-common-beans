package com.mahisoft.kamino.commonbeans.http.filter;


import com.mahisoft.kamino.commonbeans.http.Headers;
import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

public class RequestIdentifierFilter implements Filter {

    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody"})
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            String requestIdentifier = request.getHeader(Headers.REQUEST_IDENTIFIER);

            if (requestIdentifier == null) {
                requestIdentifier = UUID.randomUUID().toString();
            }
            RequestIdentifier.setIdentifier(requestIdentifier);
            MDC.put("request-identifier", requestIdentifier);
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            MDC.clear();
        }
    }

    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody"})
    public void destroy() {

    }
}
package com.mahisoft.kamino.commonbeans.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.AbstractMessageListenerContainer;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConditionalOnProperty(name = "spring.kafka.bootstrap-servers")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class MessageListenerContainerFactory<K, V> {

    private final Map<String, Object> consumerProperties;

    private String groupId;

    private String groupIdPrefix;

    private String groupIdSuffix;

    private final ObjectMapper objectMapper;

    private String[] topics;

    private MessageListener<K, V> listener;

    private Deserializer<K> keyDeserializer;

    private Deserializer<V> valueDeserializer;

    @SuppressWarnings("unchecked")
    public MessageListenerContainerFactory(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapServers,
            @Value("${spring.kafka.consumer.group-id:}") String groupId,
            @Value("${spring.kafka.consumer.auto-offset-reset:latest}") String autoOffsetReset,
            @Value("${spring.kafka.consumer.max-partition-fetch-bytes:500000}") int maxPartitionFetchBytes,
            @Value("${spring.kafka.consumer.session-timeout-ms:30000}") int sessionTimeout,
            @Value("${spring.kafka.consumer.request-timeout-ms:31000}") int requestTimeout,
            @Autowired ObjectMapper objectMapper) {

        this.groupId = groupId;
        this.objectMapper = objectMapper;

        consumerProperties = new HashMap<>();
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        consumerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        consumerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        consumerProperties.put(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG, maxPartitionFetchBytes);
        consumerProperties.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeout);
        consumerProperties.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeout);

    }

    public MessageListenerContainerFactory<K, V> keyDeserializer(Deserializer<K> keyDeserializer) {
        this.keyDeserializer = keyDeserializer;
        return this;
    }

    public MessageListenerContainerFactory<K, V> valueDeserializer(Deserializer<V> valueDeserializer) {
        this.valueDeserializer = valueDeserializer;
        return this;
    }

    public MessageListenerContainerFactory<K, V> topics(String... topics) {
        this.topics = topics;
        return this;
    }

    public MessageListenerContainerFactory<K, V> messageListener(MessageListener<K, V> listener) {
        this.listener = listener;
        return this;
    }

    public MessageListenerContainerFactory<K, V> groupIdPrefix(String value) {
        this.groupIdPrefix = value;
        return this;
    }

    public MessageListenerContainerFactory<K, V> groupIdSuffix(String suffix) {
        this.groupIdSuffix = suffix;
        return this;
    }

    public MessageListenerContainerFactory<K, V> valueType(Class<V> valueType) {
        return valueDeserializer(new JsonDeserializer<>(valueType, objectMapper));
    }

    public KafkaMessageListenerContainer<K, V> build() {
        if (Strings.isNullOrEmpty(groupId)) {
            groupId = StringUtils.join(
                    Strings.isNullOrEmpty(groupIdPrefix) ? "" : groupIdPrefix,
                    listener.getClass().getName(),
                    Strings.isNullOrEmpty(groupIdSuffix) ? "" : groupIdSuffix);
            groupId = groupId.replaceAll("[^a-zA-Z0-9._-]", "_");
        }

        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        DefaultKafkaConsumerFactory<K, V> consumerFactory = new DefaultKafkaConsumerFactory<>(
                consumerProperties,
                keyDeserializer,
                valueDeserializer
        );

        ContainerProperties containerProperties = new ContainerProperties(topics);
        containerProperties.setAckMode(AbstractMessageListenerContainer.AckMode.RECORD);
        containerProperties.setMessageListener(listener);

        return new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);
    }

}
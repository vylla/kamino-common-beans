package com.mahisoft.kamino.commonbeans.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.MessageListener;

import java.io.IOException;
import java.util.UUID;

@Slf4j
public abstract class AbstractMessageListener<K, V extends MessageBase> implements MessageListener<K, String> {

    private final ObjectMapper objectMapper;
    private final Class<V> valueType;

    protected AbstractMessageListener(ObjectMapper objectMapper, Class<V> valueType) {
        this.objectMapper = objectMapper;
        this.valueType = valueType;
    }

    @Override
    public void onMessage(ConsumerRecord<K, String> record) {
        try {
            Record<K, V> message = Record.<K, V>builder()
                    .key(record.key())
                    .timeStamp(record.timestamp())
                    .timestampType(record.timestampType())
                    .value(
                            objectMapper.readValue(record.value(), valueType)
                    )
                    .build();

            setRequestIdentifier(message);

            process(message);
        } catch (IOException e) {
            log.warn("Unable to process message: %s", record.value());
        }
    }

    private void setRequestIdentifier(Record<K, V> message) {
        if (Strings.isNullOrEmpty(message.getValue().getRequestIdentifier())) {
            message.getValue().setRequestIdentifier(UUID.randomUUID().toString());
        }

        RequestIdentifier.setIdentifier(message.getValue().getRequestIdentifier());
    }


    protected abstract void process(Record<K, V> data);

}
package com.mahisoft.kamino.commonbeans.kafka;

import com.google.common.base.Strings;
import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class Producer<K, M extends MessageBase> {

    private final KafkaTemplate<K, M> kafkaTemplate;

    @Value("${spring.kafka.producer.enabled:false}")
    private boolean isProducerActive;

    public void send(String topic, K key, M message) {
        if (!isProducerActive) {
            return;
        }
        setRequestIdentifier(message);
        log.info("Sending Message id '{}' topic '{}' key '{}'", message.getRequestIdentifier(), topic, key);
        kafkaTemplate.send(topic, message);
        log.info("Message sent id '{}' topic '{}' key '{}'", message.getRequestIdentifier(), topic, key);
    }

    private void setRequestIdentifier(M message) {
        if (Strings.isNullOrEmpty(message.getRequestIdentifier())) {
            if (Strings.isNullOrEmpty(RequestIdentifier.getIdentifier())) {
                message.setRequestIdentifier(UUID.randomUUID().toString());
            } else {
                message.setRequestIdentifier(RequestIdentifier.getIdentifier());
            }
        }
    }

}

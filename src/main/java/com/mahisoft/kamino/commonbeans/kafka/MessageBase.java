package com.mahisoft.kamino.commonbeans.kafka;

import com.mahisoft.kamino.commonbeans.http.RequestIdentifier;
import lombok.Data;

import java.time.Instant;

@Data
public class MessageBase {

    /**
     * Use the all argument constructor instead.
     * This constructor will be removed in further deliveries.
     */
    @Deprecated
    protected MessageBase() {
        this(Instant.now(), RequestIdentifier.getIdentifier());
    }

    protected MessageBase(Instant timeStamp, String requestIdentifier) {
        this.timeStamp = timeStamp;
        this.requestIdentifier = requestIdentifier;
    }

    private Instant timeStamp;

    private String requestIdentifier;
}

package com.mahisoft.kamino.commonbeans.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.apache.kafka.common.record.TimestampType;

@AllArgsConstructor
@Builder
public class Record<K, V> {

    @Getter
    private K key;

    @Getter
    private long timeStamp;

    @Getter
    private TimestampType timestampType;

    @Getter
    private V value;

}
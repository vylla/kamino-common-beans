package com.mahisoft.kamino.commonbeans.constants;

public final class Profiles {

    private Profiles() {

    }

    public static final String DEVELOPMENT = "dev";
    public static final String TEST = "test";
    public static final String PRODUCTION = "prod";
    public static final String TEST_NOAUTH = "test-noauth";
    public static final String DOC = "doc";
    public static final String CLOUD = "cloud";

}

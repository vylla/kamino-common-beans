package com.mahisoft.kamino.commonbeans.exception;

import com.mahisoft.kamino.commonbeans.http.ApiError;
import lombok.Getter;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.Charset;

@Getter
@SuppressWarnings("PMD.SingularField")
public class ApiHttpClientErrorException extends HttpClientErrorException {

    private final ApiError error;

    public ApiHttpClientErrorException(HttpClientErrorException ex, ApiError error, Charset responseCharset) {
        super(ex.getStatusCode(), ex.getStatusText(), ex.getResponseHeaders(), ex.getResponseBodyAsByteArray(), responseCharset);
        this.error = error;
    }

}
package com.mahisoft.kamino.commonbeans.exception;

import com.mahisoft.kamino.commonbeans.http.ApiError;
import lombok.Getter;
import org.springframework.web.client.HttpServerErrorException;

import java.nio.charset.Charset;

@Getter
@SuppressWarnings("PMD.SingularField")
public class ApiHttpServerErrorException extends HttpServerErrorException {

    private final ApiError error;

    public ApiHttpServerErrorException(HttpServerErrorException ex, ApiError error, Charset responseCharset){
        super(ex.getStatusCode(), ex.getStatusText(), ex.getResponseHeaders(), ex.getResponseBodyAsByteArray(), responseCharset);

        this.error = error;
    }
}
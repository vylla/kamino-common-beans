package com.mahisoft.kamino.commonbeans.exception;

public class ServiceTempUnavailableException extends RuntimeException {

    public ServiceTempUnavailableException(String message) {
        super(message);
    }
}

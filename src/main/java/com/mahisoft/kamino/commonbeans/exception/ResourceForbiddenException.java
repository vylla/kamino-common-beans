package com.mahisoft.kamino.commonbeans.exception;

public class ResourceForbiddenException extends RuntimeException {

    public ResourceForbiddenException(String message) {
        super(message);
    }
}

package com.mahisoft.kamino.commonbeans.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CustomValidationException extends RuntimeException {

    private String message;
    private Integer code;

}

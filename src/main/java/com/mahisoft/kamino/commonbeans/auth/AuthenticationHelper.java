package com.mahisoft.kamino.commonbeans.auth;

import com.nimbusds.jwt.JWTClaimsSet;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@ConditionalOnProperty(name = "oauth2.auth.jwt.keyValue")
@RequiredArgsConstructor
public class AuthenticationHelper {

    private final TokenStore tokenStore;

    private final JwtHelper jwtHelper;

    /**
     * Get client id from access token
     *
     * @return String, or null if not present
     */
    public String getClientId() {
        try {
            return getClaims().getClaim("client_id").toString();
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Gets user ID from access token
     *
     * @return user ID, or empty if not present
     */
    public Optional<Long> getUserId() {
        //TODO library methods of SPRING should use underscore but REST methods camel case. Inconsistent
        Object value = getAdditionalInformation().get(JwtAdditionalFields.USER_ID.getFieldName());
        return Optional.ofNullable(value)
                .map(v -> Long.valueOf(v.toString()));
    }

    /**
     * Gets the user domain
     *
     * @return domain if it exists
     */
    public Optional<String> getDomain() {
        try {
            Object domain = getClaims().getClaim(JwtAdditionalFields.DOMAIN.getFieldName());
            if (domain != null) {
                return Optional.of(domain.toString());
            }
            return Optional.empty();
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    /**
     * Get list of groups
     *
     * @return list of groups if they exist
     */
    public Optional<List<String>> getGroups() {
        try {
            return Optional.ofNullable(((List<String>) getClaims().getClaim(JwtAdditionalFields.GROUPS.getFieldName())));
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    /**
     * Get additional values from token
     *
     * @return Map with additional values
     */
    public Map<String, Object> getAdditionalInformation() {
        return getAccessToken().getAdditionalInformation();
    }

    private OAuth2Authentication getCurrentAuthentication() {
        return (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
    }

    private OAuth2AccessToken getAccessToken() {
        OAuth2Authentication authentication = getCurrentAuthentication();
        if (authentication == null) {
            throw new InvalidTokenException("Unable to get token information");
        }

        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
        if (details == null || details.getTokenValue() == null) {
            throw new InvalidTokenException("Unable to get token information");
        }
        return tokenStore.readAccessToken(details.getTokenValue());
    }

    private JWTClaimsSet getClaims() throws ParseException {
        return jwtHelper.getJWTClaimsFromToken(getAccessToken().getValue());
    }
}
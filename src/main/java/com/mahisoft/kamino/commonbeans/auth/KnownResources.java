package com.mahisoft.kamino.commonbeans.auth;

public class KnownResources {

    public static final String[] OPEN_RESOURCES = {
            "/health",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/**",
            "/swagger-ui.html",
            "/webjars/**",
            "/prometheus"
    };

}
package com.mahisoft.kamino.commonbeans.auth;

public enum JwtAdditionalFields {
    USER_ID("user_id"),
    DOMAIN("domain"),
    GROUPS("groups");

    private final String fieldName;

    JwtAdditionalFields(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}

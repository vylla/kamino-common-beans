package com.mahisoft.kamino.commonbeans.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@ConditionalOnProperty(
        name = {"oauth2.auth.jwt.keyValue"}
)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    //Shared public jwt signature key
    @Value("${oauth2.auth.jwt.keyValue}")
    private String jwtPublicKey;

    /**
     * For other services to decode token and check for groups
     * @return
     * @throws Exception
     */
    @Bean
    @Primary
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    protected TokenStore tokenStore() throws Exception {
        JwtAccessTokenConverter enhancer = new JwtAccessTokenConverter();
        enhancer.setVerifierKey(this.jwtPublicKey);
        enhancer.afterPropertiesSet();
        return new JwtTokenStore(enhancer);
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(null).tokenStore(tokenStore());
    }
}
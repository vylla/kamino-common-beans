package com.mahisoft.kamino.commonbeans.config;

import com.google.common.base.Predicates;
import com.mahisoft.kamino.commonbeans.constants.Profiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.CollectionUtils;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@Profile(Profiles.DOC)
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api(DocumentationProperties documentationProperties) {

        ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any());

        if (!CollectionUtils.isEmpty(documentationProperties.getExclude())) {
            documentationProperties
                    .getExclude()
                    .forEach(x ->
                            builder.apis(
                                    Predicates.not(RequestHandlerSelectors.basePackage(x))
                            ));
        }

        return builder.build();
    }
}

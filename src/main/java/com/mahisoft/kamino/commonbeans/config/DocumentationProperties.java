package com.mahisoft.kamino.commonbeans.config;

import com.mahisoft.kamino.commonbeans.constants.Profiles;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Profile(Profiles.DOC)
@Component
@ConfigurationProperties(prefix = "kamino.commons.swagger")
public class DocumentationProperties {

    private List<String> exclude = new ArrayList<>();

}

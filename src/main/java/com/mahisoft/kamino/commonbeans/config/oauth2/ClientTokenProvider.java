package com.mahisoft.kamino.commonbeans.config.oauth2;


import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

public class ClientTokenProvider extends ClientCredentialsAccessTokenProvider {

    private final RestTemplate restTemplate;

    public ClientTokenProvider(RestTemplate restTemplate) {
        super();
        this.restTemplate = restTemplate;
        setMessageConverters(restTemplate.getMessageConverters());
    }

    @Override
    protected RestOperations getRestTemplate() {
        return restTemplate;
    }
}

package com.mahisoft.kamino.commonbeans.config.oauth2;

import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RequestAuthenticator;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.StringUtils;


public class RequestAuthenticator implements OAuth2RequestAuthenticator {

	@Override
	public void authenticate(
			OAuth2ProtectedResourceDetails resource,
			OAuth2ClientContext clientContext,
			ClientHttpRequest request) {

		OAuth2AccessToken accessToken = clientContext.getAccessToken();
		if (accessToken == null) {
			throw new AccessTokenRequiredException(resource);
		}

		String tokenType = accessToken.getTokenType();
		if (!StringUtils.hasText(tokenType) || OAuth2AccessToken.BEARER_TYPE.equalsIgnoreCase(tokenType)) {
			tokenType = OAuth2AccessToken.BEARER_TYPE;
		}

		String authorization = tokenType + ' ' + accessToken.getValue();
		request.getHeaders().set(HttpHeaders.AUTHORIZATION, authorization);
	}
}

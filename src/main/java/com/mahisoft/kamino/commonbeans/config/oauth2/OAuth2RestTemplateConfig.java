package com.mahisoft.kamino.commonbeans.config.oauth2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;

@EnableOAuth2Client
@Configuration
@ConditionalOnProperty(prefix = "security.oauth2.client", name = "client-id")
public class OAuth2RestTemplateConfig {

    @Bean
    public OAuth2RestTemplateBuilder oauth2RestTemplateBuilder(
            @Qualifier("restTemplate") RestTemplate restTemplate) {
        return new OAuth2RestTemplateBuilder(null, null, restTemplate);
    }

    @Bean
    public OAuth2RestTemplate clientCredentialsOAuth2RestTemplate(
            OAuth2RestTemplateBuilder builder,
            ClientCredentialsResourceDetails resource) {
        return builder.setResource(resource).build();
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    @Primary
    public DefaultOAuth2ClientContext requestOAuth2ClientContext() {
        DefaultOAuth2ClientContext context = new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest());
        Authentication principal = SecurityContextHolder.getContext()
                .getAuthentication();
        if (principal instanceof OAuth2Authentication) {
            OAuth2Authentication authentication = (OAuth2Authentication) principal;
            Object details = authentication.getDetails();
            if (details instanceof OAuth2AuthenticationDetails) {
                OAuth2AuthenticationDetails oauthsDetails = (OAuth2AuthenticationDetails) details;
                String token = oauthsDetails.getTokenValue();
                context.setAccessToken(new DefaultOAuth2AccessToken(token));
            }
        }

        return context;
    }

    @Bean
    @ConditionalOnProperty(prefix = "security.oauth2.resource.jwt", name = "key-value")
    public OAuth2RestTemplate tokenRelayOAuth2RestTemplate(
            OAuth2RestTemplateBuilder builder,
            ClientCredentialsResourceDetails resource,
            OAuth2ClientContext context) {
        return builder.setResource(resource).setContext(context).build();
    }

}

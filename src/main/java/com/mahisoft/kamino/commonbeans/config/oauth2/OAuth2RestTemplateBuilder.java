package com.mahisoft.kamino.commonbeans.config.oauth2;

import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
public class OAuth2RestTemplateBuilder {

    private ClientCredentialsResourceDetails resource;
    private OAuth2ClientContext context;
    private RestTemplate restTemplate;

    public OAuth2RestTemplateBuilder setResource(ClientCredentialsResourceDetails resource) {
        return new OAuth2RestTemplateBuilder(resource, this.context, this.restTemplate);
    }

    public OAuth2RestTemplateBuilder setContext(OAuth2ClientContext context) {
        return new OAuth2RestTemplateBuilder(this.resource, context, this.restTemplate);
    }

    public OAuth2RestTemplate build() {
        OAuth2RestTemplate oauth2RestTemplate = (context == null) ?
                new OAuth2RestTemplate(resource) :
                new OAuth2RestTemplate(resource, context);

        oauth2RestTemplate.setRequestFactory(restTemplate.getRequestFactory());
        oauth2RestTemplate.setMessageConverters(restTemplate.getMessageConverters());
        oauth2RestTemplate.setAccessTokenProvider(new ClientTokenProvider(restTemplate));
        oauth2RestTemplate.setAuthenticator(new RequestAuthenticator());
        return oauth2RestTemplate;
    }
}

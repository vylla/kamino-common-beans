package com.mahisoft.kamino.commonbeans.config;


import com.mahisoft.kamino.commonbeans.http.filter.RequestIdentifierFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RequestIdentifierFilterConfig {

    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(requestIdentifierFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setName("requestIdentifierFilter");
        return registrationBean;
    }

    @Bean(name = "requestIdentifierFilter")
    public RequestIdentifierFilter requestIdentifierFilter(){
        return new RequestIdentifierFilter();
    }
}
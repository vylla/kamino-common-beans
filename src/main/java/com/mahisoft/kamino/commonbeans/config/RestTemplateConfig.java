package com.mahisoft.kamino.commonbeans.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mahisoft.kamino.commonbeans.http.handler.HttpResponseErrorHandler;
import com.mahisoft.kamino.commonbeans.http.interceptor.HeaderRequestInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Bean
    @Primary
    public RestTemplate restTemplate(RestTemplateBuilder builder, ObjectMapper objectMapper, HeaderRequestInterceptor interceptor){
        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
                .setMaxConnPerRoute(20)
                .setMaxConnTotal(200)
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClient);


        RestTemplate restTemplate = builder
                .errorHandler(new HttpResponseErrorHandler(objectMapper))
                .requestFactory(requestFactory)
                .build();
        restTemplate.getInterceptors().add(interceptor);

        for (HttpMessageConverter converter : restTemplate.getMessageConverters()) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter c = (MappingJackson2HttpMessageConverter) converter;
                c.setObjectMapper(objectMapper);
            }
        }

        return restTemplate;
    }



}

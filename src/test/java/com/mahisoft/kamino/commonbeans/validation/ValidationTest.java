package com.mahisoft.kamino.commonbeans.validation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.Validation;
import javax.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ValidationTest {

    private static Validator validator;

    @Before
    public void beforeClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    class TestClass {
        @NullOrNotBlank
        private String testValue;
    }

    @Test
    public void setValueNull() {
        TestClass testClass = new TestClass();
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);
        assertEquals("size", 0, violations.size());
        Assert.assertNull("Test Value should be null", testClass.getTestValue());
    }

    @Test
    public void setValueEmpty() {
        TestClass testClass = new TestClass("");
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);
        assertEquals("size", 1, violations.size());
        assertEquals("message", "The value is empty or blank.", violations.iterator().next().getMessage());
    }

    @Test
    public void setValueAllBlank() {
        TestClass testClass = new TestClass("           ");
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);
        assertEquals("size", 1, violations.size());
        assertEquals("message", "The value is empty or blank.", violations.iterator().next().getMessage());
    }
}


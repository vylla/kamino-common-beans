package com.mahisoft.kamino.commonbeans.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = DemoApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class Oauth2RestTemplateTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("clientCredentialsOAuth2RestTemplate")
    private OAuth2RestTemplate clientCredentialsRestTemplate;

    @Autowired
    @Qualifier("tokenRelayOAuth2RestTemplate")
    private RestTemplate tokenRelayRestTemplate;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private String getInternalFile(String path) throws IOException {
        ClassPathResource cpr = new ClassPathResource(path);
        byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
        return new String(bdata, StandardCharsets.UTF_8);
    }


    private String getClientAccessToken() throws IOException {
        Map map = (Map) this.objectMapper.readValue(this.getInternalFile("token/client_token.json"), Map.class);
        return (String) map.get("access_token");
    }

    private String getUserAccessToken() throws IOException {
        Map map = (Map) this.objectMapper.readValue(this.getInternalFile("token/user_token.json"), Map.class);
        return (String) map.get("access_token");
    }

    @Test
    public void shouldGetAccessToken() throws IOException, InterruptedException {
        MockRestServiceServer authServer = MockRestServiceServer.createServer(restTemplate);
        authServer.expect(requestTo("http://localhost:9001/api/oauth/token"))
                .andRespond(withSuccess("{\"access_token\":\"ACCESS_TOKEN\"}", MediaType.APPLICATION_JSON));

        MockRestServiceServer otherService = MockRestServiceServer.createServer(clientCredentialsRestTemplate);
        otherService.expect(requestTo(AsyncService.URL))
                .andExpect(header(HttpHeaders.AUTHORIZATION, "Bearer ACCESS_TOKEN"))
                .andRespond(withSuccess("{\"body\": 'hello'}", MediaType.APPLICATION_JSON));

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + getUserAccessToken());

        ResponseEntity<String> response = testRestTemplate.exchange(
                "/async",
                HttpMethod.POST,
                new HttpEntity<>(headers),
                String.class
        );

        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
        TimeUnit.SECONDS.sleep(1);

        authServer.verify();
        otherService.verify();
    }

    @Test
    public void relayTokenAutomatically() throws IOException, InterruptedException {
        String authorization = "Bearer " + getUserAccessToken();

        MockRestServiceServer otherService = MockRestServiceServer.createServer(tokenRelayRestTemplate);

        otherService.expect(requestTo(RelayService.URL))
                .andExpect(header(HttpHeaders.AUTHORIZATION, authorization))
                .andRespond(withSuccess("{\"id\": 123}", MediaType.APPLICATION_JSON));

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, authorization);

        ResponseEntity<String> response = testRestTemplate.exchange(
                "/relay",
                HttpMethod.GET,
                new HttpEntity<>(headers),
                String.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        otherService.verify();
    }

    @Test
    public void relayTokenOnePerRequest() throws IOException {
        String authorization1 = "Bearer " + getClientAccessToken();
        String authorization2 = "Bearer " + getUserAccessToken();

        MockRestServiceServer service1 = MockRestServiceServer.createServer(tokenRelayRestTemplate);
        service1.expect(requestTo(RelayService.URL))
                .andExpect(header(HttpHeaders.AUTHORIZATION, authorization1))
                .andRespond(withSuccess("{\"id\": 123}", MediaType.APPLICATION_JSON));
        service1.expect(requestTo(RelayService.URL))
                .andExpect(header(HttpHeaders.AUTHORIZATION, authorization2))
                .andRespond(withSuccess("{\"id\": 123}", MediaType.APPLICATION_JSON));

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, authorization1);

        ResponseEntity<String> response = testRestTemplate.exchange(
                "/relay",
                HttpMethod.GET,
                new HttpEntity<>(headers),
                String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        HttpHeaders headers2 = new HttpHeaders();
        headers2.add(HttpHeaders.AUTHORIZATION, authorization2);

        ResponseEntity<String> response2 = testRestTemplate.exchange(
                "/relay",
                HttpMethod.GET,
                new HttpEntity<>(headers2),
                String.class);

        assertEquals(HttpStatus.OK, response2.getStatusCode());
        service1.verify();
    }
}

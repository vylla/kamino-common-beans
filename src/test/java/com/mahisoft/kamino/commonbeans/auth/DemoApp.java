package com.mahisoft.kamino.commonbeans.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.mahisoft.kamino.commonbeans" })
@SuppressWarnings({ "PMD.UseUtilityClass" })
public class DemoApp {

    public static void main(String[] args) {
        new SpringApplication(DemoApp.class).run(args);
    }
}

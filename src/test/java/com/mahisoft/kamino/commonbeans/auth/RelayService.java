package com.mahisoft.kamino.commonbeans.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;

/**
 * Service that invokes another service.
 */
@Component
@Slf4j
public class RelayService {

    public static final String URL = "http://ms-something/hello";

    @Autowired
    @Qualifier("tokenRelayOAuth2RestTemplate")
    private OAuth2RestTemplate restTemplate;

    public String relay() {
        log.info("Authentication {}", SecurityContextHolder.getContext().getAuthentication());
        return restTemplate.getForObject(URL, String.class);
    }
}

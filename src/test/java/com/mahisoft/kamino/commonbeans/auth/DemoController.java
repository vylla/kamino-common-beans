package com.mahisoft.kamino.commonbeans.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example controller.
 */
@RestController
@Slf4j
public class DemoController {

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private RelayService relayService;

    @RequestMapping(value = "/async", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void async() {
        asyncService.async();
    }

    @RequestMapping(value = "/relay", method = RequestMethod.GET)
    public String relay() {
        return relayService.relay();
    }
}

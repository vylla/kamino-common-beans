package com.mahisoft.kamino.commonbeans.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;

/**
 * Service that implements an asynchronous method.
 */
@Component
@Slf4j
public class AsyncService {

    public static final String URL = "http://ms-something/hello";

    @Autowired
    @Qualifier("clientCredentialsOAuth2RestTemplate")
    private OAuth2RestTemplate restTemplate;

    @Async
    public void async() {
        log.info("authentication {}", SecurityContextHolder.getContext().getAuthentication());
        restTemplate.getForObject(URL, String.class);
    }
}

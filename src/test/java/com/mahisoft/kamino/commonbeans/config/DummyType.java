package com.mahisoft.kamino.commonbeans.config;

public enum  DummyType {
    Yes,
    No,
    Maybe;
}

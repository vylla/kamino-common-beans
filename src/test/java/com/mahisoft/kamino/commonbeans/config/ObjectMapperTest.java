package com.mahisoft.kamino.commonbeans.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;

public class ObjectMapperTest {

    private final static Long TIME = 1362484800L;

    @Test
    public void testObjectMapperConfiguration() throws IOException, JSONException {

        ObjectMapper mapper = new ObjectMapperConfig().objectMapper();

        URL url = Resources.getResource("config/object-mapper.json");
        String expectedJson = Resources.toString(url, Charsets.UTF_8);

        String dummyJson = mapper.writeValueAsString(
                Dummy.builder()
                        .someLongValue(1L)
                        .someStringValue("Hello World!")
                        .someInstantValue(Instant.ofEpochMilli(TIME))
                        .someEnumValue(DummyType.Yes)
                        .build());

        JSONAssert.assertEquals(expectedJson, dummyJson, true);
    }

}

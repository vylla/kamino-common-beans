package com.mahisoft.kamino.commonbeans.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Dummy {

    private String someStringValue;

    private Long someLongValue;

    private Instant someInstantValue;

    private String alwaysNullValue;

    private DummyType someEnumValue;

}

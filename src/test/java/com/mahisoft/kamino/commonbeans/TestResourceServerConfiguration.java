package com.mahisoft.kamino.commonbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
@SuppressWarnings({ "PMD.SignatureDeclareThrowsException" })
public class TestResourceServerConfiguration extends ResourceServerConfigurerAdapter {

   @Autowired
   private TokenStore tokenStore;

   @Override
   public void configure(HttpSecurity http) throws Exception {
      http.csrf()
              .disable().authorizeRequests()
              .antMatchers("/error**").permitAll()
              .antMatchers("/**").authenticated();
   }

   @Override
   public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
      resources.resourceId(null).tokenStore(tokenStore);
   }
}

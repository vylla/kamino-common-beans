package com.mahisoft.kamino.commonbeans.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = ExceptionApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class RestExceptionsTest {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Value("classpath:exception/exception.json")
    protected File exception;

    @Value("classpath:exception/known-exception.json")
    protected File knownException;

    @Value("classpath:exception/custom-exception.json")
    protected File customException;

    @Test
    public void shouldThrowException() throws InterruptedException, IOException, JSONException {
        MockRestServiceServer server = MockRestServiceServer.createServer(restTemplate);

        ResponseEntity<String> response = testRestTemplate.exchange(
                "/error-unknown",
                HttpMethod.GET,
                new HttpEntity<>(null),
                String.class
        );

        ResponseEntity<String> response2 = testRestTemplate.exchange(
                "/error-known",
                HttpMethod.GET,
                new HttpEntity<>(null),
                String.class
        );

        ResponseEntity<String> response3 = testRestTemplate.exchange(
                "/error-custom",
                HttpMethod.GET,
                new HttpEntity<>(null),
                String.class
        );

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(HttpStatus.PRECONDITION_FAILED, response2.getStatusCode());
        assertEquals(HttpStatus.PRECONDITION_FAILED, response3.getStatusCode());

        JSONAssert.assertEquals(FileUtils.readFileToString(exception, Charset.defaultCharset()), response.getBody(), false);
        JSONAssert.assertEquals(FileUtils.readFileToString(knownException, Charset.defaultCharset()), response2.getBody(),   false);
        JSONAssert.assertEquals(FileUtils.readFileToString(customException, Charset.defaultCharset()), response3.getBody(), false);

        TimeUnit.SECONDS.sleep(1);
        server.verify();
    }

}

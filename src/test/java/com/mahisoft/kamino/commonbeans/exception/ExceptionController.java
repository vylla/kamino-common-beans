package com.mahisoft.kamino.commonbeans.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example controller.
 */
@RestController
@Slf4j
@SuppressWarnings({"PMD.SignatureDeclareThrowsException", "PMD.AvoidThrowingRawExceptionTypes"})
public class ExceptionController {

    @RequestMapping(value = "/error-unknown", method = RequestMethod.GET)
    public String throwError() throws Exception {
        throw new Exception("Some message");
    }

    @RequestMapping(value = "/error-known", method = RequestMethod.GET)
    public String throwError2() {
        throw new IllegalStateException("Some illegal state message");
    }

    @RequestMapping(value = "/error-custom", method = RequestMethod.GET)
    public String throwError4() throws Exception {
        throw new CustomValidationException("Some custom exception", 90033);
    }
}

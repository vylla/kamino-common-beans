package com.mahisoft.kamino.commonbeans.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.mahisoft.kamino.commonbeans" })
@SuppressWarnings({ "PMD.UseUtilityClass" })
public class ExceptionApp {

    public static void main(String[] args) {
        new SpringApplication(ExceptionApp.class).run(args);
    }
}

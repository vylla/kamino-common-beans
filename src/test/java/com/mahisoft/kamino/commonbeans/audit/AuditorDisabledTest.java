package com.mahisoft.kamino.commonbeans.audit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.AuditorAware;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;

@TestPropertySource(locations = "classpath:no-audit/application.yml")
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AuditApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class AuditorDisabledTest {

    @Autowired
    private AuditorAware<Long> auditor;

    @Test
    public void testAuditorDisabled() {
        Assert.assertThat("Auditor returns 0", auditor.getCurrentAuditor(), is(0L));
    }

}

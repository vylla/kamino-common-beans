package com.mahisoft.kamino.commonbeans.audit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.mahisoft.kamino.commonbeans" })
@SuppressWarnings({ "PMD.UseUtilityClass" })
public class AuditApp {

    public static void main(String[] args) {
        new SpringApplication(AuditApp.class).run(args);
    }
}
